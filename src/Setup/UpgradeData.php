<?php

namespace Cream\DutchShop\Setup;

use Cream\DutchShop\Setup\Upgrader\Cms as CmsUpgrader;
use Cream\DutchShop\Setup\Upgrader\Tax as TaxUpgrader;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CmsUpgrader
     */
    private $cmsUpgrader;

    /**
     * @var TaxUpgrader
     */
    private $taxUpgrader;

    /**
     * UpgradeData constructor.
     * @param CmsUpgrader $cmsUpgrader
     * @param TaxUpgrader $taxUpgrader
     */
    public function __construct(
        CmsUpgrader $cmsUpgrader,
        TaxUpgrader $taxUpgrader
    ) {
        $this->cmsUpgrader = $cmsUpgrader;
        $this->taxUpgrader = $taxUpgrader;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->cmsUpgrader->upgrade($setup, $context);
        $this->taxUpgrader->upgrade($setup, $context);
    }
}
