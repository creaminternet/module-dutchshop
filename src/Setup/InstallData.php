<?php

namespace Cream\DutchShop\Setup;

use Cream\DutchShop\Setup\Installer\Configuration as ConfigurationInstaller;
use Cream\DutchShop\Setup\Installer\Tax as TaxInstaller;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * @var ConfigurationInstaller
     */
    private $configurationInstaller;

    /**
     * @var TaxInstaller
     */
    private $taxInstaller;

    /**
     * @param ConfigurationInstaller $configurationInstaller
     * @param TaxInstaller           $taxInstaller
     */
    public function __construct(ConfigurationInstaller $configurationInstaller, TaxInstaller $taxInstaller)
    {
        $this->configurationInstaller = $configurationInstaller;
        $this->taxInstaller = $taxInstaller;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->taxInstaller->install($setup, $context);
        $this->configurationInstaller->install($setup, $context);
    }
}
