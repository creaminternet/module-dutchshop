<?php

namespace Cream\DutchShop\Setup\Upgrader;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\App\State;
use Magento\Tax\Model\Calculation\RateRepository as TaxRateRepository;
use Magento\Tax\Model\ResourceModel\Calculation\Rate\CollectionFactory as TaxRateCollectionFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Area;

class Tax implements UpgradeDataInterface
{
    const LOW_TAX_RATE_CODE = 'NL-BTW-LAAG';

    /**
     * @var State
     */
    private $appState;

    /**
     * @var TaxRateRepository
     */
    private $taxRateRepository;

    /**
     * @var TaxRateCollectionFactory
     */
    private $taxRateCollectionFactory;

    /**
     * @param State $appState
     * @param TaxRateRepository $taxRateRepository
     * @param TaxRateCollectionFactory $taxRateCollectionFactory
     */
    public function __construct(
        State $appState,
        TaxRateRepository $taxRateRepository,
        TaxRateCollectionFactory $taxRateCollectionFactory
    ) {
        $this->appState = $appState;
        $this->taxRateRepository = $taxRateRepository;
        $this->taxRateCollectionFactory = $taxRateCollectionFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->appState->emulateAreaCode(Area::AREA_ADMINHTML, function () use ($setup, $context) {
            if (version_compare($context->getVersion(), '1.0.11') < 0) {
                $this->updateDutchTaxRate(self::LOW_TAX_RATE_CODE, 6, 9);
            }
        });
    }

    /**
     * Update the dutch tax rate
     * @param string $rateCode
     * @param int $oldRate
     * @param int $newRate
     */
    private function updateDutchTaxRate($rateCode, $oldRate, $newRate)
    {
        $taxRates = $this->taxRateCollectionFactory->create();
        $taxRates->addFieldToFilter('code', ['eq' => $rateCode]);
        $taxRates->addFieldToFilter('rate', ['eq' => $oldRate]);

        if (!$taxRates || $taxRates->getSize() <= 0) {
            return;
        }

        foreach ($taxRates as $taxRate) {
            $taxRate->setRate($newRate);
            $this->taxRateRepository->save($taxRate);
        }
    }
}
