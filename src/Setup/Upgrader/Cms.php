<?php

namespace Cream\DutchShop\Setup\Upgrader;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Cms\Model\ResourceModel\Page\Collection as PageCollection;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model\Page;
use Magento\Cms\Model\Block;
use Psr\Log\LoggerInterface;

class Cms implements UpgradeDataInterface
{
    /**
     * Original URL keys.
     */
    const URL_KEYS = [
        'no-route',
        'home',
        'enable-cookies',
        'privacy-policy-cookie-restriction-mode',
        'about-us',
        'customer-service'
    ];

    /**
     * Blocks that we replace.
     */
    const BLOCKS = [
        'footer_links_block'
    ];

    /**
     * XML path to page and block configuration.
     */
    const XML_PATH_PAGES  = 'dutchshop/pages';
    const XML_PATH_BLOCKS = 'dutchshop/blocks';

    /**
     * @var PageCollection
     */
    private $pageCollection;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param PageCollection $pageCollection
     * @param PageFactory $pageFactory
     * @param BlockFactory $blockFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $logger
     */
    public function __construct(
        PageCollection $pageCollection,
        PageFactory $pageFactory,
        BlockFactory $blockFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger
    ) {
        $this->pageCollection = $pageCollection->addFieldToFilter('identifier', ['in' => self::URL_KEYS]);
        $this->pageFactory = $pageFactory;
        $this->blockFactory = $blockFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $this->disableOriginalCmsPages();
        }

        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $this->createDutchPages();
        }

        if (version_compare($context->getVersion(), '1.0.10') < 0) {
            $this->updateBlocks();
        }
    }

    /**
     * Disables the original (english) cms pages.
     *
     * @TODO: As of writing this (Magento 2.1.1), we have to directly use a collection instead of repository
     *        because the repository's getList method returns data arrays without ids while its save method
     *        requires a PageInterface. We may need to revisit this as Magento becomes more mature...
     */
    private function disableOriginalCmsPages()
    {
        foreach ($this->pageCollection as $cmsPage) {
            $cmsPage->setIsActive(false);
        }

        $this->pageCollection->save();
    }

    /**
     * Creates new dutch CMS pages from configuration.
     */
    private function createDutchPages()
    {
        $pages = $this->scopeConfig->getValue(self::XML_PATH_PAGES);

        foreach ($pages as $urlKey => $pageData) {
            /** @var Page $page */
            $page = $this->pageFactory->create();
            $page->addData($pageData);
            $page->setIdentifier($urlKey);
            $page->setStoreId(0);

            try {
                $page->save();
            } catch (\Exception $exception) {
                $this->logger->alert("Could not save page: " . $exception->getMessage());
            }
        }
    }

    /**
     * Replaces hardcoded static blocks with dutch values.
     */
    private function updateBlocks()
    {
        foreach (self::BLOCKS as $blockId) {
            /** @var Block $block */
            $block = $this->blockFactory->create()->load($blockId, 'identifier');

            if ($block->getId()) {
                $block->setContent($this->scopeConfig->getValue(self::XML_PATH_BLOCKS . '/' . $blockId));
                try {
                    $block->save();
                } catch (\Exception $exception) {
                    $this->logger->alert("Could not save block: " . $exception->getMessage());
                }
            }
        }
    }
}
