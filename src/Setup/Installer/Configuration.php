<?php

namespace Cream\DutchShop\Setup\Installer;

use Magento\Config\Model\Config;
use Magento\Config\Model\ConfigFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Tax\Model\ResourceModel\TaxClass\Collection as TaxClassCollection;

class Configuration implements InstallDataInterface
{

    /**
     * @var ConfigFactory
     */
    private $configFactory;

    /**
     * @var TaxClassCollection
     */
    private $taxClassCollection;

    /**
     * @param ConfigFactory $configFactory
     * @param TaxClassCollection $taxClassCollection
     */
    public function __construct(ConfigFactory $configFactory, TaxClassCollection $taxClassCollection)
    {
        $this->configFactory = $configFactory;
        $this->taxClassCollection = $taxClassCollection;
    }

    /**
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        foreach ($this->getConfigData() as $path => $value) {
            /* @var Config $config */
            $config = $this->configFactory->create();
            $config->setDataByPath($path, $value);
            $config->save();
        }
    }

    /**
     * These settings get written to core_config_data by the installer, so we can't use config.xml for these.
     *
     * @return array
     */
    private function getConfigData()
    {
        $taxClassId = $this->getDefaultTaxClassId();
        return [
            'general/locale/code'      => 'nl_NL',
            'currency/options/base'    => 'EUR',
            'currency/options/default' => 'EUR',
            'currency/options/allow'   => 'EUR',
            'general/locale/timezone'  => 'Europe/Amsterdam',
            'tax/classes/shipping_tax_class' => $taxClassId,
            'tax/classes/default_product_tax_class'  => $taxClassId
        ];
    }

    /**
     * @return int
     */
    private function getDefaultTaxClassId()
    {
        $this->taxClassCollection->addFieldToFilter('class_name', ['eq' => 'BTW Hoog']);
        return $this->taxClassCollection->getFirstItem()->getData('class_id');
    }
}
