<?php

namespace Cream\DutchShop\Setup\Installer;

use Magento\Framework\App\State;
use Magento\Framework\Module\Setup\Context;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Tax\Model\Calculation\RateFactory as TaxRateFactory;
use Magento\Tax\Model\Calculation\RateRepository as TaxRateRepository;
use Magento\Tax\Model\Calculation\RuleFactory as TaxRuleFactory;
use Magento\Tax\Model\ClassModel as TaxClassModel;
use Magento\Tax\Model\ClassModelFactory as TaxClassFactory;
use Magento\Tax\Model\ResourceModel\TaxClass\CollectionFactory as TaxClassCollectionFactory;
use Magento\Tax\Model\TaxClass\Repository as TaxClassRepository;
use Magento\Tax\Model\TaxRuleRepository;
use Magento\Framework\App\Area;
use Psr\Log\LoggerInterface;

class Tax implements InstallDataInterface
{
    /**
     * @var State
     */
    private $appState;

    /**
     * @var TaxRateRepository
     */
    private $taxRateRepository;

    /**
     * @var TaxRateFactory
     */
    private $taxRateFactory;

    /**
     * @var TaxRuleRepository
     */
    private $taxRuleRepository;

    /**
     * @var TaxRuleFactory
     */
    private $taxRuleFactory;

    /**
     * @var TaxClassCollectionFactory
     */
    private $taxClassCollectionFactory;

    /**
     * @var TaxClassRepository
     */
    private $taxClassRepository;

    /**
     * @var TaxClassFactory
     */
    private $taxClassFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param State $appState
     * @param Context $context
     * @param TaxRateRepository $taxRateRepository
     * @param TaxRateFactory $taxRateFactory
     * @param TaxRuleRepository $taxRuleRepository
     * @param TaxRuleFactory $taxRuleFactory
     * @param TaxClassCollectionFactory $taxClassCollectionFactory
     * @param TaxClassRepository $taxClassRepository
     * @param TaxClassFactory $taxClassFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        State $appState,
        Context $context,
        TaxRateRepository $taxRateRepository,
        TaxRateFactory $taxRateFactory,
        TaxRuleRepository $taxRuleRepository,
        TaxRuleFactory $taxRuleFactory,
        TaxClassCollectionFactory $taxClassCollectionFactory,
        TaxClassRepository $taxClassRepository,
        TaxClassFactory $taxClassFactory,
        LoggerInterface $logger
    ) {
        $this->appState = $appState;
        $this->taxRateRepository = $taxRateRepository;
        $this->taxRateFactory = $taxRateFactory;
        $this->taxRuleRepository = $taxRuleRepository;
        $this->taxRuleFactory = $taxRuleFactory;
        $this->taxClassCollectionFactory = $taxClassCollectionFactory;
        $this->taxClassRepository = $taxClassRepository;
        $this->taxClassFactory = $taxClassFactory;
        $this->logger = $logger;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->appState->emulateAreaCode(Area::AREA_ADMINHTML, function () use ($setup, $context) {
            $taxRateIds = $this->createDutchTaxRates();
            $taxClassIds = $this->createDutchTaxClasses();
            $this->createDutchTaxRules($taxClassIds, $taxRateIds);
        });
    }

    /**
     * @return array
     */
    private function getTaxRates()
    {
        return [
            'NL-BTW-HOOG' => [ 'tax_country_id' => 'NL', 'tax_postcode' => '*', 'rate' => '21' ],
            'NL-BTW-LAAG' => [ 'tax_country_id' => 'NL', 'tax_postcode' => '*', 'rate' => '9'  ],
            'NL-BTW-GEEN' => [ 'tax_country_id' => 'NL', 'tax_postcode' => '*', 'rate' => '0'  ]
        ];
    }

    /**
     * @return array
     */
    private function getTaxClasses()
    {
        return [
            'Geen BTW' => 'NL-BTW-GEEN',
            'BTW Laag' => 'NL-BTW-LAAG',
            'BTW Hoog' => 'NL-BTW-HOOG'
        ];
    }

    /**
     * @param string $classType
     * @return TaxClassModel
     */
    private function getClassModelByClassType($classType)
    {
        $collection = $this->taxClassCollectionFactory->create();
        $collection->addFieldToFilter('class_type', $classType);
        return $collection->getFirstItem();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function createDutchTaxRates()
    {
        $taxRateIds = [];
        foreach ($this->getTaxRates() as $code => $data) {
            $taxRate = $this->taxRateFactory->create();
            $taxRate->setCode($code);
            $taxRate->addData($data);
            try {
                $this->taxRateRepository->save($taxRate);
            } catch (\Exception $exception) {
                $this->logger->alert("Could not create tax rate: " . $exception->getMessage());
            }
            $taxRateIds[$code] = $taxRate->getId();
        }
        return $taxRateIds;
    }

    /**
     * @param $productTaxClassIds
     * @param $taxRateIds
     * @return array
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function createDutchTaxRules($productTaxClassIds, $taxRateIds)
    {
        $taxRules = [];
        $taxClasses = $this->getTaxClasses();
        $customerClassModel = $this->getClassModelByClassType(TaxClassModel::TAX_CLASS_TYPE_CUSTOMER);
        foreach (array_keys($this->getTaxClasses()) as $ruleCode) {
            $taxRule = $this->taxRuleFactory->create();
            $taxRule->setCode($ruleCode);
            $taxRule->addData(
                [
                'priority'               => 0,
                'position'               => 0,
                'calculate_subtotal'     => 0,
                'customer_tax_class_ids' => [ $customerClassModel->getId() ],
                'product_tax_class_ids'  => [ $productTaxClassIds[$ruleCode] ],
                'tax_rate_ids'           => [ $taxRateIds[$taxClasses[$ruleCode]] ]
                ]
            );
            try {
                $taxRules[$ruleCode] = $this->taxRuleRepository->save($taxRule);
            } catch (\Exception $exception) {
                $this->logger->alert("Could not create tax rule: " . $exception->getMessage());
            }
        }
        return $taxRules;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function createDutchTaxClasses()
    {
        $taxClassIds = [];
        foreach (array_keys($this->getTaxClasses()) as $ruleCode) {
            $taxClass = $this->taxClassFactory->create();
            $taxClass->setClassType(TaxClassModel::TAX_CLASS_TYPE_PRODUCT);
            $taxClass->setClassName($ruleCode);
            try {
                $this->taxClassRepository->save($taxClass);
            } catch (\Exception $exception) {
                $this->logger->alert("Could not create tax class: " . $exception->getMessage());
            }
            $taxClassIds[$ruleCode] = $taxClass->getId();
        }
        return $taxClassIds;
    }
}
