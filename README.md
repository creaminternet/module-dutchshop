# Magento 2 Dutch Shop
 
When setting up a new Magento 2 store for the dutch market, several changes need to be made to your installation. This module aims to ease that process by providing sensible defaults for configuration settings, tax settings, etc.

# What it does #

1. This repository includes a [dutch language pack](https://packagist.org/packages/creaminternet/language-nl_nl) and [dutch (belgium) language pack](https://packagist.org/packages/creaminternet/language-nl_be) by default.
2. Default Magento configuration is [overridden](https://bitbucket.org/creaminternet/module-dutchshop/src/master/etc/config.xml) to provide sensible dutch default values. These values can still be changed in the backend.
3. Tax rules, tax rates and tax classes are [created](https://bitbucket.org/creaminternet/module-dutchshop/src/master/Setup/Installer/Tax.php) and properly associated. 

# Release Notes #

## Release 1.4.0

- Improvement: Added Dutch (Belgium) Language pack .
- Improvement: Added Dutch (Belgium) Language Pack for 3rd party modules.
- Improvement: Deleted composer.lock file from project as it's not necessary.

## Release 1.3.7.1 ##

- Fix: Update README with the latest changes.

## Release 1.3.7 ##

- Added more standard tax settings.

## Release 1.3.6 ##

- Added exception handling for saving the CMS pages and blocks. 

## Release 1.3.5 ##

- CREAM-351: Added exception handling when saving a tax rule or class fails. If an item already exists it will no longer fail the upgrade.

## Release 1.3.4 ##

- Modified Install and Update scripts to set the low tax percentage to 9%.
- Removed use of setAreaCode which results in bugs during setup:upgrade.

## Release 1.3.3 ##
   
 - Removed PHP version constraint.

# Installation #
 
## Via composer ##
 
To install this package with composer you need access to the command line of your server and you need to have Composer. Install with the following commands:
 
```bash
cd <your magento path>
composer require creaminternet/module-dutchshop:@stable
php bin/magento module:enable Cream_DutchShop
php bin/magento setup:upgrade
php bin/magento cache:flush
php bin/magento setup:static-content:deploy nl_NL
```
 
## Manually ##
 
To install this package manually you need access to your server file system and you need access to the command line of your server. And take the following steps:
 
1. Download the zip file from the Bitbucket repository.
2. Upload the contents to ``<your magento path>/app/code/Cream/DutchShop``.
3. Execute the following commands:
 
```bash
cd <your magento path>
php bin/magento module:enable Cream_DutchShop
php bin/magento setup:upgrade
php bin/magento cache:flush
php bin/magento setup:static-content:deploy nl_NL
```
 
# Contribute #
 
To help push the Dutch Shop module forward please fork the Bitbucket repository and submit a pull request with your changes.
 
# License #

This module is distributed under the following licenses:
 
* Academic Free License ("AFL") v. 3.0
* Open Software License v. 3.0 (OSL-3.0)
 
# Authors #
 
* [Cream](https://www.cream.nl/)
 
# About Cream #
 
Cream is an ecommerce solution provider for the Magento platform. You'll find an overview of all our (open source) projects [on our website](https://www.cream.nl/).
